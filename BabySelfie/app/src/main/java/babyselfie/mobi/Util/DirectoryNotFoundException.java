package babyselfie.mobi.Util;

/**
 * Created by Francisco on 07/03/2017.
 */

public class DirectoryNotFoundException extends Exception{
    public DirectoryNotFoundException(String message){
        super(message);
    }
}
