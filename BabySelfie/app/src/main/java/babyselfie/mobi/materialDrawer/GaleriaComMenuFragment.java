package babyselfie.mobi.materialDrawer;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.GridView;

import com.R;
import babyselfie.mobi.classes.ListAdapter;
import babyselfie.mobi.bot.GaleriaAdapter;
import babyselfie.mobi.bot.ImageItem;
import babyselfie.mobi.bot.MovieCreator;
import babyselfie.mobi.bot.PicturesRepository;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link GaleriaComMenuFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link GaleriaComMenuFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class GaleriaComMenuFragment extends Fragment {

    private GridView mGridview;
    private GaleriaAdapter mGaleriaAdapter;
    private ListAdapter mListAdapter;
    Button mBtnGerarFilme;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public GaleriaComMenuFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment GaleriaComMenuFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static GaleriaComMenuFragment newInstance(String param1, String param2) {
        GaleriaComMenuFragment fragment = new GaleriaComMenuFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_galeria_com_menu, container, false);

        //getData();
        mGridview = (GridView) v.findViewById(R.id.gvGaleria);
        // Com holder
        //mGaleriaAdapter = new GaleriaAdapter(this.getActivity(), R.layout.item_galeria, PicturesRepository.getInstance().getList());
        PicturesRepository.getInstance().getFromSdCard();

        ArrayList<String> items = new ArrayList<String>();

        for (int p = 0; p < 100; p++) {
            items.add("photo");
        }

        mListAdapter = new ListAdapter(this.getActivity(), items);

        mGaleriaAdapter = new GaleriaAdapter(this.getActivity(), R.layout.item_galeria, PicturesRepository.getInstance().getFromSdCard());
        mGridview.setAdapter(mGaleriaAdapter);
//        mGridview.setAdapter(mListAdapter);
        mBtnGerarFilme = (Button) v.findViewById(R.id.btGerar);
        Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Hanken-Light.ttf");
        mBtnGerarFilme.setTypeface(typeface);

        mBtnGerarFilme.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MovieCreator teste = new MovieCreator(getActivity());
                teste.execute("foda-se");
            }
        });
        return v;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            /*
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
                    */
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private ArrayList<ImageItem> getData() {
        final ArrayList<ImageItem> imageItems = new ArrayList<ImageItem>();
        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.sample_0);
        Bitmap bitmap2 = BitmapFactory.decodeResource(getResources(), R.drawable.sample_1);
        Bitmap bitmap3 = BitmapFactory.decodeResource(getResources(), R.drawable.sample_2);
        for(int i = 0; i <=100000; i++){
            PicturesRepository.getInstance().addPicture(new ImageItem(bitmap));
            PicturesRepository.getInstance().addPicture(new ImageItem(bitmap2));
            PicturesRepository.getInstance().addPicture(new ImageItem(bitmap3));
            //imageItems.add(new ImageItem(bitmap));
            //imageItems.add(new ImageItem(bitmap2));
            //imageItems.add(new ImageItem(bitmap3));
        }
        return imageItems;
    }
}
