package babyselfie.mobi.materialDrawer;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.R;
import babyselfie.mobi.Util.CameraUtil;
import babyselfie.mobi.Util.DirectoryNotFoundException;
import babyselfie.mobi.bot.CameraActivity;
import babyselfie.mobi.bot.Constants;
import babyselfie.mobi.bot.Tela3FrequenciaActivity;
import babyselfie.mobi.bot.Tela9VideoActivity;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.display.CircleBitmapDisplayer;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.nostra13.universalimageloader.utils.DiskCacheUtils;
import com.nostra13.universalimageloader.utils.MemoryCacheUtils;
import babyselfie.mobi.repository.Fachada;

import org.json.JSONObject;

import java.io.File;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by Francisco on 01/11/2016.
 */

public class MenuActivity extends AppCompatActivity {
    private DrawerLayout mDrawer;
    private Toolbar toolbar;
    private NavigationView nvDrawer;
    private ActionBarDrawerToggle mDrawerToggle;
    private CircleImageView mIvPerfil;
    private TextView mTvNomeUsuario;
    private TextView mTvEmailUsuario;
    private Button mBtnConfirm;
    private Button mBtnCancel;
    private TextView mTvDlgApagar;
    private NavigationView mHeader;
    private Context context;
    //    private AlertDialog confirmationDialog;
    private Dialog confirmationDialog;
    private Fachada fachada;
    private JSONObject jsonObject;
    private JSONObject fbUserPhoto;
    private JSONObject profile_pic_url;
    private DisplayImageOptions options;
    private ImageView mIvTeste;
    private ImageLoadingListener animateFirstListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Hanken-Light.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );


        initImageLoader(getApplicationContext());

        //Método utilziado para ajustar o perfil no header
        setHeaderProfile();

        // Set a Toolbar to replace the ActionBar.
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Find our drawer view
        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerToggle = setupDrawerToggle();

        // Tie DrawerLayout events to the ActionBarToggle
        mDrawer.addDrawerListener(mDrawerToggle);

        // Find our drawer view
        nvDrawer = (NavigationView) findViewById(R.id.nvView);

        //mIvPerfil.setLayoutParams(new LinearLayout.LayoutParams(imageWidthColumn, imageWidthColumn));
        // Setup drawer view
        setupDrawerContent(nvDrawer);

        loadFirstFragment();
    }

    private void loadFirstFragment() {

        String defaultTitle = getString(R.string.galeria);
        Fragment fragment = null;
        //Alterei aqui
//        Class fragmentClass = GaleriaComMenuFragment.class;
        Class fragmentClass = GaleriaFragment2.class;
        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.flContent, fragment).commit();


        setTitle(defaultTitle);

    }

    private void setHeaderProfile() {
        //TODO pegar isso da conta do facebook ou cadastro do firebase
        mHeader = (NavigationView) findViewById(R.id.nvView);
        View header = mHeader.getHeaderView(0);
        mTvNomeUsuario = (TextView) header.findViewById(R.id.tvHeaderName);
        mTvEmailUsuario = (TextView) header.findViewById(R.id.tvHeaderEmail);
        mIvPerfil = (CircleImageView) header.findViewById(R.id.ivHeaderFoto);
        mIvTeste = (ImageView) header.findViewById(R.id.ivTeste);
        animateFirstListener = new AnimateFirstDisplayListener();



        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.mipmap.ic_update)
                .showImageForEmptyUri(R.mipmap.ic_update)
                .showImageOnFail(R.mipmap.ic_update)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .displayer(new CircleBitmapDisplayer(Color.WHITE, 5))
                .build();

        if(accessToken != null){
            GraphRequest data_request = GraphRequest.newMeRequest(
                    accessToken,
                    new GraphRequest.GraphJSONObjectCallback() {
                        @Override
                        public void onCompleted(
                                JSONObject json_object,
                                GraphResponse response) {

                            jsonObject = json_object;

                            try {
                                mTvNomeUsuario.setText(json_object.get("email").toString());
                                mTvEmailUsuario.setText(json_object.get("name").toString());
                                fbUserPhoto =  new JSONObject(json_object.get("picture").toString());
                                profile_pic_url = new JSONObject(fbUserPhoto.getString("data"));

//                                Bitmap profilePic= BitmapFactory.decodeStream(profile_pic_url.toString() .openConnection().getInputStream());
                                mIvPerfil.setImageResource(R.drawable.baby_selfie_logo);
                                //TODO pegar a imagem de perfil do fb
//                                ImageLoader.getInstance().displayImage(profile_pic_url.getString("url"), mIvTeste, options, animateFirstListener);

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
//                            Intent intent = new Intent(MainActivity.this,HomeActivity.class);
//                            intent.putExtra("jsondata",json_object.toString());
//                            startActivity(intent);
                        }
                    });
            Bundle permission_param = new Bundle();
            permission_param.putString("fields", "id,name,email,picture");
            data_request.setParameters(permission_param);
            data_request.executeAsync();
        }
        else{
            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
            if (user != null) {
                // Name, email address, and profile photo Url
                mTvNomeUsuario.setText(user.getDisplayName());
                mTvEmailUsuario.setText(user.getEmail());
                mIvPerfil.setImageResource(R.drawable.baby_selfie_logo);
            }
        }
    }

    private ActionBarDrawerToggle setupDrawerToggle() {
        return new ActionBarDrawerToggle(this, mDrawer, toolbar, R.string.drawer_open, R.string.drawer_close);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // The action bar home/up action should open or close the drawer.
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawer.openDrawer(GravityCompat.START);
                return true;
        }
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    // `onPostCreate` called when activity start-up is complete after `onStart()`
    // NOTE! Make sure to override the method with only a single `Bundle` argument
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        selectDrawerItem(menuItem);
                        return true;
                    }
                });
    }

    public void selectDrawerItem(MenuItem menuItem) {
        // Create a new fragment and specify the fragment to show based on nav item clicked
        //TODO alteras as transições
        Fragment fragment = null;
        Class fragmentClass;
        fragmentClass = GaleriaFragment2.class;
        boolean finish = false;
        boolean doNothing = false;
        Intent intent = null;

        switch (menuItem.getItemId()) {
            case R.id.nav_first_fragment:
//                fragmentClass = NovoPeriodoFragment.class;
                intent = new Intent(MenuActivity.this, Tela3FrequenciaActivity.class);
                intent.putExtra(Constants.ALTERAR_FREQUENCIA, true);
                finish = true;
                break;
            case R.id.nav_second_fragment:
//                fragmentClass = ReproduzirVideoFragment.class;
                File video =  new File("file://" + CameraUtil.getVideosDirectory() + "video.mp4");
                if (!video.exists()){
//                    Toast.ma
                }
                intent = new Intent(MenuActivity.this, Tela9VideoActivity.class);
                finish = true;
                break;
            case R.id.nav_third_fragment:
                fragmentClass = GaleriaFragment2.class;
                doNothing = true;
                callDialog();
//                fragmentClass = ApagarFotosFragment.class;
                break;
            case R.id.nav_fourth_fragment:
                intent = new Intent(MenuActivity.this, CameraActivity.class);
                finish = true;
//                fragmentClass = MudarInformacoesFragment.class;
                break;
            default:
                fragmentClass = MudarInformacoesFragment.class;
        }

        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if(!doNothing && !finish){
            // Insert the fragment by replacing any existing fragment
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.flContent, fragment).commit();
        }
        else if(!doNothing && intent != null){
            startActivity(intent);
            //finish();
        }
        // Highlight the selected item has been done by NavigationView
        menuItem.setChecked(true);
        // Set action bar title
        setTitle(menuItem.getTitle());
        // Close the navigation drawer
        mDrawer.closeDrawers();
    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggles
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    public static void initImageLoader(Context context) {
        ImageLoaderConfiguration.Builder config = new ImageLoaderConfiguration.Builder(context);
        config.threadPriority(Thread.NORM_PRIORITY - 2);
        config.denyCacheImageMultipleSizesInMemory();
        config.diskCacheFileNameGenerator(new Md5FileNameGenerator());
        config.diskCacheSize(50 * 1024 * 1024); // 50 MiB
        config.tasksProcessingOrder(QueueProcessingType.LIFO);
        config.writeDebugLogs(); // Remove for release app
        ImageLoader.getInstance().init(config.build());
    }

    private void callDialog() {
        context = this;
        fachada = new Fachada();

        confirmationDialog = new Dialog(context);
        confirmationDialog.setContentView(R.layout.dialog_apagar_todas);
        mBtnConfirm = (Button) confirmationDialog.findViewById(R.id.btnDlgConfirm);
        mBtnCancel = (Button) confirmationDialog.findViewById(R.id.btnDlgCancel);
        mBtnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    //Limpando a cache
                    ImageLoader.getInstance().clearMemoryCache();
                    ImageLoader.getInstance().clearDiskCache();
                    ImageLoader.getInstance().getMemoryCache();
                    ImageLoader.getInstance().getDiskCache();

                    List<String> deletedFiles = fachada.deleteAllPictures();
                    Intent mediaStoreUpdateIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                    for (String path : deletedFiles) {
                        mediaStoreUpdateIntent.setData(Uri.parse(path));
                        Log.d("Path do scan: ", path);
                        sendBroadcast(mediaStoreUpdateIntent);
                        MemoryCacheUtils.removeFromCache(path, ImageLoader.getInstance().getMemoryCache());
                        DiskCacheUtils.removeFromCache(path, ImageLoader.getInstance().getDiskCache());
                    }

                } catch (DirectoryNotFoundException e) {
                    //TODO
//                    Toast.makeText();
                }
                confirmationDialog.dismiss();
                //Recarregar a página

                Intent intent = new Intent(MenuActivity.this, MenuActivity.class);
                startActivity(intent);
                finish();

            }
        });
        mBtnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                confirmationDialog.dismiss();
            }
        });

        confirmationDialog.show();
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    private static class AnimateFirstDisplayListener extends SimpleImageLoadingListener {

        static final List<String> displayedImages = Collections.synchronizedList(new LinkedList<String>());

        @Override
        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
            if (loadedImage != null) {
                ImageView imageView = (ImageView) view;
                boolean firstDisplay = !displayedImages.contains(imageUri);
                if (firstDisplay) {
                    FadeInBitmapDisplayer.animate(imageView, 500);
                    displayedImages.add(imageUri);
                }
            }
        }
    }
}

