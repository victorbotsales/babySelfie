package babyselfie.mobi.materialDrawer;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.R;

import babyselfie.mobi.bot.Constants;
import babyselfie.mobi.bot.MovieCreator;
import babyselfie.mobi.fragment.AbsListViewBaseFragment;
import com.example.bot.babyselfie.PhotoDetailActivity;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingProgressListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import babyselfie.mobi.repository.Repositorio;

/**
 * Created by Francisco on 05/03/2017.
 */

public class GaleriaFragment2 extends AbsListViewBaseFragment {

    public static final int INDEX = 1;
    Button mBtnGerarFilme;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_galeria_com_menu, container, false);
        listView = (GridView) rootView.findViewById(R.id.gvFgtGaleria);
        ((GridView) listView).setAdapter(new ImageAdapter(getActivity()));
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                startImagePagerActivity(position);
//                parent.getChildAt(position);
//                listView.getAdapter().getItem(position);
                Object object = listView.getItemAtPosition(position);
                Log.d("avohay", "ah foi em paz");
            }
        });

        mBtnGerarFilme = (Button) rootView.findViewById(R.id.btGerar);
        Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Hanken-Light.ttf");
        mBtnGerarFilme.setTypeface(typeface);

        mBtnGerarFilme.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MovieCreator teste = new MovieCreator(getActivity());
                /* Execução em paralelo utilizando AsyncTask
                 */
                //teste.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                /* Execução em uma única Thread utilizando AsyncTask!
                 */
                teste.execute("Criando um vídeo");
                /* Execução utilizando um serviço (IntentService)
                 */
//                getActivity().startService(new Intent(getActivity(), MovieCreatorService.class));

                 /* Execução utilizando um serviço (Service)
                 */
//                getActivity().startService(new Intent(getActivity(), MovieCreatorService2.class));

            }
        });
        return rootView;
    }

    private void startImagePagerActivity(int position) {
        Intent intent = new Intent(getActivity(), PhotoDetailActivity.class);
        intent.putExtra(Constants.IMG_POSITION, position);
        getActivity().startActivity(intent);
//        String URI = ar
//        intent.putExtra()
    }

    private static class ImageAdapter extends BaseAdapter {

        private static final String[] IMAGE_URLS = Repositorio.getInstance().getAllImgUri();//Constants.IMAGES;

        private LayoutInflater inflater;

        private DisplayImageOptions options;

        ImageAdapter(Context context) {
            inflater = LayoutInflater.from(context);

            //TODO alterar os ícones
            options = new DisplayImageOptions.Builder()
                    .showImageOnLoading(R.mipmap.ic_update)
                    .showImageForEmptyUri(R.mipmap.ic_update)
                    .showImageOnFail(R.mipmap.ic_update)
                    .cacheInMemory(true)
                    .cacheOnDisk(true)
                    .considerExifParams(true)
                    .bitmapConfig(Bitmap.Config.RGB_565)
                    .build();
        }

        @Override
        public int getCount() {
            return IMAGE_URLS.length;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final ViewHolder holder;
            View view = convertView;
            if (view == null) {
                view = inflater.inflate(R.layout.item_grid_image, parent, false);
                holder = new ViewHolder();
                assert view != null;
                holder.imageView = (ImageView) view.findViewById(R.id.image);
                holder.progressBar = (ProgressBar) view.findViewById(R.id.progress);
                view.setTag(holder);
            } else {
                holder = (ViewHolder) view.getTag();
            }

            ImageLoader.getInstance()
                    .displayImage(IMAGE_URLS[position], holder.imageView, options, new SimpleImageLoadingListener() {
                        @Override
                        public void onLoadingStarted(String imageUri, View view) {
                            holder.progressBar.setProgress(0);
                            holder.progressBar.setVisibility(View.VISIBLE);
                        }

                        @Override
                        public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                            holder.progressBar.setVisibility(View.GONE);
                        }

                        @Override
                        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                            holder.progressBar.setVisibility(View.GONE);
                        }
                    }, new ImageLoadingProgressListener() {
                        @Override
                        public void onProgressUpdate(String imageUri, View view, int current, int total) {
                            holder.progressBar.setProgress(Math.round(100.0f * current / total));
                        }
                    });

            return view;
        }
    }

    static class ViewHolder {
        ImageView imageView;
        ProgressBar progressBar;
    }
}