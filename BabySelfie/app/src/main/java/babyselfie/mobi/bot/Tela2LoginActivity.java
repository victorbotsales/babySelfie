package babyselfie.mobi.bot;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.R;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class Tela2LoginActivity extends AppCompatActivity {
    private static final int REQUEST_SIGNUP = 2;
    private EditText etEmail;
    private EditText etSenha;
    private TextView tvCadastra;
    private TextView mTvEsqueciSenha;
    private Button btLogin;
    private Button btLoginFace;
    private FirebaseAuth mAuth;
    private LoginButton loginButton;
    private CallbackManager callbackManager;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private AccessTokenTracker mAccessTokenTracker;
    private AccessToken mAccessToken;
    private FirebaseUser mUser;


    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Hanken-Light.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_tela2loginv2);

        //isLogged();

        mAuth = FirebaseAuth.getInstance();

        setUpAuthStateListener();

        mUser = FirebaseAuth.getInstance().getCurrentUser();

        btLogin = (Button) findViewById(R.id.btLogin);
        btLoginFace = (Button) findViewById(R.id.btEntrarFacebook);
        tvCadastra = (TextView) findViewById(R.id.tvCadastra);
        mTvEsqueciSenha = (TextView) findViewById(R.id.tvEsqueciSenha);

        mTvEsqueciSenha.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                recuperarSenha();
            }
        });

        setUpFacebookLogin();

        if(isLoggedIn()){
            onLoginSuccess();
        }

        btLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login();
            }
        });

        btLoginFace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO utilizar api do facebook
                /*
                    TODO Adicionar esses argumentos abaixo para ambos os logins, colocar esse código no onLoginSucess() de ambos (Facebook e Normal)
                 */

                loginButton.performClick();


                /*
                SharedPreferences prefs = getSharedPreferences("config", MODE_PRIVATE);
                int frequencia = prefs.getInt("frequencia",-1);
                int horarioHoras = prefs.getInt("horarioHoras",-1);
                int horarioMinutos = prefs.getInt("horarioMinutos",-1);
                Log.v("frequencia",Integer.toString(frequencia));
                Log.v("horarioHoras",Integer.toString(horarioHoras));
                Log.v("horarioMinutos",Integer.toString(horarioMinutos));
                if(frequencia>0){
                    Intent it = new Intent(Tela2LoginActivity.this, MainActivity.class);
                    startActivity(it);
                }
                else {
                    Intent it = new Intent(Tela2LoginActivity.this, Tela3FrequenciaActivity.class);
                    startActivity(it);
                }
                */
            }
        });

        tvCadastra.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Tela2LoginActivity.this, CadastroActivity.class);
                startActivityForResult(intent, REQUEST_SIGNUP);

            }
        });



    }

    private void setUpAuthStateListener() {
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // User is signed in
                    Log.d("user not null", "onAuthStateChanged:signed_in:" + user.getUid());
                } else {
                    // User is signed out
                    Log.d("user null", "onAuthStateChanged:signed_out");
                }
            }
        };

    }

    private void recuperarSenha(){
        //TODO ajeitae/fazer callback pra essa função
        etEmail = (EditText) findViewById(R.id.etEmail);
        etSenha = (EditText) findViewById(R.id.etSenha);

        String email = etEmail.getText().toString();

        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            etEmail.setError(getString(R.string.digite_um_email_valido));
        } else {
            mAuth.sendPasswordResetEmail(email);
            etEmail.setError(null);
        }


    }


    private void setUpFacebookLogin() {

        callbackManager = CallbackManager.Factory.create();

        mAccessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(
                    AccessToken oldAccessToken,
                    AccessToken currentAccessToken) {
                // Set the access token using
                // currentAccessToken when it's loaded or set.
            }
        };
        // If the access token is available already assign it.
        mAccessToken = AccessToken.getCurrentAccessToken();

        loginButton = (LoginButton) findViewById(R.id.btnFbLogin);
        loginButton.setReadPermissions("email", "public_profile");

        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.i("success", "loginButton");
            }

            @Override
            public void onCancel() {
                Log.i("cancel", "loginButton");
            }

            @Override
            public void onError(FacebookException exception) {
                Log.i("error", "loginButton");
            }
        });


        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        // App code

                        AccessToken.getCurrentAccessToken();

                        mAccessToken = AccessToken.getCurrentAccessToken();
                        handleFacebookAccessToken();
                        Log.i("success", "loginManager");
                        onLoginSuccess();
                    }

                    @Override
                    public void onCancel() {
                        // App code
                        Log.i("cancel", "manager");
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        LoginManager.getInstance().logOut();
                        Log.i("error", "manager");
                        // App code
                        onLoginFailed();
                    }
                });
    }

    public boolean hasPreferences(){

        SharedPreferences prefs = getSharedPreferences("config", MODE_PRIVATE);
        int frequencia = prefs.getInt("frequencia",-1);
        int horarioHoras = prefs.getInt("horarioHoras",-1);
        int horarioMinutos = prefs.getInt("horarioMinutos",-1);
        Log.v("frequencia",Integer.toString(frequencia));
        Log.v("horarioHoras",Integer.toString(horarioHoras));
        Log.v("horarioMinutos",Integer.toString(horarioMinutos));
        if(frequencia>0){
            Log.i("tem", "preferencia");
            return true;
            //Intent it = new Intent(Tela2LoginActivity.this, MainActivity.class);
            //startActivity(it);
        }
        else {
            Log.i("naum tem", "preferencia");
            return false;
            //Intent it = new Intent(Tela2LoginActivity.this, Tela3FrequenciaActivity.class);
            //startActivity(it);
        }
    }

    public void login() {

        if (!validar()) {
            onLoginFailed();
            return;
        }

        etEmail = (EditText) findViewById(R.id.etEmail);
        etSenha = (EditText) findViewById(R.id.etSenha);
        btLogin.setEnabled(false);

        final ProgressDialog progressDialog = new ProgressDialog(Tela2LoginActivity.this,
                R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(getString(R.string.autenticando));
        progressDialog.show();

        String email = etEmail.getText().toString();
        String senha = etSenha.getText().toString();

        // TODO: Implementar autenticação

        mAuth.signInWithEmailAndPassword(email, senha)
                .addOnCompleteListener(Tela2LoginActivity.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        //progressBar.setVisibility(View.GONE);

                        if (!task.isSuccessful()) {
                            // there was an error
                                Toast.makeText(Tela2LoginActivity.this, getString(R.string.auth_failed), Toast.LENGTH_LONG).show();
                                btLogin.setEnabled(true);

                        } else {
                            onLoginSuccess();
                        }
                        progressDialog.dismiss();

                    }
                });

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    private void handleFacebookAccessToken() {
        Log.d("teste auth fb", "handleFacebookAccessToken:" + mAccessToken.getToken());

        AuthCredential credential = FacebookAuthProvider.getCredential(mAccessToken.getToken());
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d("", "signInWithCredential:onComplete:" + task.isSuccessful());

                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        if (!task.isSuccessful()) {
                            Log.w("", "signInWithCredential", task.getException());
                            Toast.makeText(Tela2LoginActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        mAccessTokenTracker.stopTracking();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }


    @Override
    public void onBackPressed() {
        // Disable going back to the MainActivity
        moveTaskToBack(true);
    }

    public void onLoginSuccess() {
        Intent it;
        if(hasPreferences()){

            it = new Intent(Tela2LoginActivity.this, CameraActivity.class);
//            it = new Intent(Tela2LoginActivity.this, MenuActivity.class);
//            it = new Intent(Tela2LoginActivity.this, ShareVideoActivity.class);
            startActivity(it);
            finish();
        }
        else{
            it = new Intent(Tela2LoginActivity.this, Tela3FrequenciaActivity.class);
            startActivity(it);
            finish();
        }

    }

    public void onLoginFailed() {
        Toast.makeText(getBaseContext(), getString(R.string.login_falhou), Toast.LENGTH_LONG).show();

        btLogin.setEnabled(true);
    }

    public boolean validar() {
        boolean valid = true;

        etEmail = (EditText) findViewById(R.id.etEmail);
        etSenha = (EditText) findViewById(R.id.etSenha);

        String email = etEmail.getText().toString();
        String password = etSenha.getText().toString();

        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            etEmail.setError(getString(R.string.digite_um_email_valido));
            valid = false;
        } else {
            etEmail.setError(null);
        }

        if (password.isEmpty() || password.length() < 6 || password.length() > 10) {
            etSenha.setError(getString(R.string.entre_4_e_10_c));
            valid = false;
        } else {
            etSenha.setError(null);
        }

        return valid;
    }
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private boolean isLoggedIn(){
        //TODO checar o login do firebase com algum cookie
        if(mAccessToken != null || mUser!=null){
            return true;
        }
        return false;
    }
}
