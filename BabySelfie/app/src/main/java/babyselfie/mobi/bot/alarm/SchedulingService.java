package babyselfie.mobi.bot.alarm;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.app.NotificationCompat;

import com.R;
import babyselfie.mobi.bot.CameraActivity;


/**
 * This {@code IntentService} does the app's actual work.
 * {@code AlarmReceiver} (a {@code WakefulBroadcastReceiver}) holds a
 * partial wake lock for this service while the service does its work. When the
 * service is finished, it calls {@code completeWakefulIntent()} to release the
 * wake lock.
 */
public class SchedulingService extends IntentService {
    public SchedulingService() {
        super("SchedulingService");
    }

    public static final int NOTIFICATION_ID = 1;
    private NotificationManager mNotificationManager;
    @Override
    protected void onHandleIntent(Intent intent) {
        sendNotification(getString(R.string.ola_vamos_tirar_uma_foto));
        // Release the wake lock provided by the BroadcastReceiver.
        AlarmReceiver.completeWakefulIntent(intent);
        // END_INCLUDE(service_onhandle)
    }
    
    // Post a notification indicating whether a doodle was found.
    private void sendNotification(String msg) {

        mNotificationManager = (NotificationManager)
                this.getSystemService(Context.NOTIFICATION_SERVICE);
        Intent mainIntent = new Intent(this, CameraActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, mainIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        Bitmap largeIcon = BitmapFactory.decodeResource(
                getResources(), R.mipmap.babyselfie_launcher);
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this);
        mBuilder.setContentText(msg);
        mBuilder.setSmallIcon(R.mipmap.babyselfie_launcher);
        mBuilder.setLargeIcon(largeIcon);
        mBuilder.setContentIntent(pendingIntent);
        //builder.setColor(Color.argb(0,210,245,255))
        mBuilder.setContentTitle("BabySelfie");
        mBuilder.setAutoCancel(true);
        mBuilder.setVibrate(new long[]{100, 500, 200, 800});
        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());

    }


}
