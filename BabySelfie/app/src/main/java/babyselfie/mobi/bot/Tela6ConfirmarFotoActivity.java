package babyselfie.mobi.bot;

import android.content.Context;
import android.content.Intent;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.R;
import babyselfie.mobi.materialDrawer.MenuActivity;
import babyselfie.mobi.repository.Fachada;
import babyselfie.mobi.repository.IFachada;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class Tela6ConfirmarFotoActivity extends AppCompatActivity {
    private Button mBtnConfirmar;
    private Button mBtnTirarOutra;
    private ImageView mIvFotoConfirma;
    private IFachada fachada;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Hanken-Light.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
        setContentView(R.layout.activity_tela6confirmarfoto);

        fachada = new Fachada();
        mBtnConfirmar = (Button)findViewById(R.id.btnConfirmar);
        mBtnTirarOutra = (Button)findViewById(R.id.btnTirarOutra);
        mIvFotoConfirma = (ImageView) findViewById(R.id.ivFotoConfirma);
        final String photoUri = getIntent().getStringExtra(Constants.URI_STRING_CODE);

        if(photoUri != "" || photoUri != null){
            mIvFotoConfirma.setImageBitmap(fachada.getImageFromSd(photoUri));
        }

        mBtnTirarOutra.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                //TODO deletar através da fachada
                if(fachada.deleteImage(photoUri)){
                    Intent mediaStoreUpdateIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                    mediaStoreUpdateIntent.setData(Uri.parse("file://" + photoUri));
                    sendBroadcast(mediaStoreUpdateIntent);
                }

                Intent it = new Intent(Tela6ConfirmarFotoActivity.this, CameraActivity.class);
                startActivity(it);
                finish();
            }
        });

        mBtnConfirmar.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                String[] path = new String[]{"file://" + photoUri};

                MediaScannerConnection.scanFile(getApplicationContext(),  path, null, new MediaScannerConnection.OnScanCompletedListener() {
                    /*
                     *   (non-Javadoc)
                     * @see android.media.MediaScannerConnection.OnScanCompletedListener#onScanCompleted(java.lang.String, android.net.Uri)
                     */
                    public void onScanCompleted(String path, Uri uri)
                    {
                        Log.i("ExternalStorage", "Scanned " + path + ":");
                        Log.i("ExternalStorage", "-> uri=" + uri);
                        Intent it = new Intent(Tela6ConfirmarFotoActivity.this, MenuActivity.class);
                        startActivity(it);
                        finish();
                    }
                });


            }
        });
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

}
