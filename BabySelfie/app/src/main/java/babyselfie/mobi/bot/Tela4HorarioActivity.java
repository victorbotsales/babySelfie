package babyselfie.mobi.bot;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.R;
import babyselfie.mobi.circularseekbar.CircularSeekBar;
import babyselfie.mobi.bot.alarm.AlarmReceiver;
import babyselfie.mobi.materialDrawer.MenuActivity;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class Tela4HorarioActivity extends AppCompatActivity {

    private Button btIniciar;
    private TextView tvProgressoHorario;
    private int minutos;
    private int horas;
    private AlarmReceiver alarm = new AlarmReceiver();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela4horario);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Hanken-Light.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );

        CircularSeekBar seekbar = (CircularSeekBar) findViewById(R.id.circularSeekBar2);
        seekbar.setMax(1439);

        //Inicializando o circularSeekBar
        seekbar.setProgress(0);
        Typeface custom_font = Typeface.createFromAsset(getAssets(), "fonts/AllerDisplay.ttf");
        tvProgressoHorario = (TextView) findViewById(R.id.tvProgressoHorario);
        tvProgressoHorario.setText("00:00");
        tvProgressoHorario.setTypeface(custom_font);
        seekbar.setOnSeekBarChangeListener(new Tela4HorarioActivity.CircleSeekBarListener());

        btIniciar = (Button) findViewById(R.id.btIniciar);
        btIniciar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Ir para tela de tirar foto ( utilizar Câmera )
                SharedPreferences prefs = getSharedPreferences("config", MODE_PRIVATE);
                SharedPreferences.Editor editor = prefs.edit();
                editor.putInt("horarioHoras",horas);
                editor.putInt("horarioMinutos",minutos);
                editor.commit();
                int frequencia = prefs.getInt("frequencia",1);
                alarm.setAlarm(Tela4HorarioActivity.this,horas,minutos,frequencia);
                Intent it;
                if (getIntent().hasExtra(Constants.ALTERAR_FREQUENCIA)){
                    it = new Intent(Tela4HorarioActivity.this, MenuActivity.class);
                }
                else{
                    it = new Intent(Tela4HorarioActivity.this, CameraActivity.class);
                }
                startActivity(it);
            }
        });
    }

    public class CircleSeekBarListener implements CircularSeekBar.OnCircularSeekBarChangeListener {
        @Override
        public void onProgressChanged(CircularSeekBar circularSeekBar, int progress, boolean fromUser) {

            minutos = progress%60;
            horas = progress/60;

            String minutosString = "";
            String horasString = "";

            if(minutos<10){
                minutosString = ":0" + Integer.toString(minutos);
            }
            else{
                minutosString = ":" + Integer.toString(minutos);
            }

            if(horas<10){
                horasString = "0" + Integer.toString(horas);
            }
            else{
                horasString =  Integer.toString(horas);
            }


            tvProgressoHorario.setText(horasString + minutosString);
        }

        @Override
        public void onStopTrackingTouch(CircularSeekBar seekBar) {

        }

        @Override
        public void onStartTrackingTouch(CircularSeekBar seekBar) {

        }
    }
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
