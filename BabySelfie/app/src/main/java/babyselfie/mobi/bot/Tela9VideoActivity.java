package babyselfie.mobi.bot;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.MediaController;
import android.widget.Toast;
import android.widget.VideoView;

import com.R;
import babyselfie.mobi.Util.CameraUtil;
import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.share.model.ShareVideo;
import com.facebook.share.model.ShareVideoContent;
import com.facebook.share.widget.ShareButton;
import com.facebook.share.widget.ShareDialog;
import babyselfie.mobi.materialDrawer.MenuActivity;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class Tela9VideoActivity extends AppCompatActivity {

    private Button mBtCompartilhar;
    private CallbackManager callbackManager;
    private ShareDialog shareDialog;
    private ShareButton mShareButton;
    private Button mBtVoltarGaleria;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Hanken-Light.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
        setContentView(R.layout.activity_tela9video);

        VideoView videoView = (VideoView)this.findViewById(R.id.vvVideo);
        MediaController mc = new MediaController(this);
        videoView.setMediaController(mc);
        mc.setAnchorView(this.findViewById(R.id.layoutVideo));
        videoView.setVideoPath(CameraUtil.getVideosDirectory() +"/video.mp4");
        videoView.requestFocus();
        videoView.start();
        setVideoShare();

        mBtVoltarGaleria = (Button) findViewById(R.id.btVoltarGaleria);

        mBtVoltarGaleria.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Tela9VideoActivity.this, MenuActivity.class);
                startActivity(intent);
            }
        });

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);

    }


    private void setVideoShare() {



        mBtCompartilhar = (Button) findViewById(R.id.btCompartilhar);
        FacebookSdk.sdkInitialize(getApplicationContext());
//        facebook
        callbackManager = CallbackManager.Factory.create();
        shareDialog = new ShareDialog(this);
//        LoginManager.getInstance().logInWithPublishPermissions(
//                this,
//                Arrays.asList("publish_actions"));
//        shareDialog.registerCallback();

        Uri videoUrl = Uri.parse("file://" + CameraUtil.getVideosDirectory().getPath() + "/video.mp4");
        ShareVideo video = new ShareVideo.Builder()
                .setLocalUrl(videoUrl)
                .build();
        final ShareVideoContent content = new ShareVideoContent.Builder()
                .setVideo(video)
                .build();

//        shareDialog.show(content);

        mShareButton = (ShareButton) findViewById(R.id.btFbShareVideo);
        mShareButton.setShareContent(content);

        mBtCompartilhar.setOnClickListener(new View.OnClickListener() {
            String facebookPackageName = "com.facebook.katana";
            String facebookClassName = "com.facebook.katana.LoginActivity";

            @Override
            public void onClick(View view) {
                try {
                    ApplicationInfo facebookAppInfo = getPackageManager()
                            .getApplicationInfo(facebookPackageName, 0);
                    Intent intent = new Intent("android.intent.category.LAUNCHER");
                    intent.setClassName(facebookPackageName, facebookClassName);
                    startActivity(intent);
                } catch (PackageManager.NameNotFoundException e) {
                    // Didn't installed
                    Toast.makeText(getApplicationContext(), R.string.facebookNotFound, Toast.LENGTH_LONG).show();
                    Uri uri = Uri.parse("market://details?id=" + facebookPackageName);
                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                    startActivity(intent);
                }
                mShareButton.performClick();
            }
        });
    }
}