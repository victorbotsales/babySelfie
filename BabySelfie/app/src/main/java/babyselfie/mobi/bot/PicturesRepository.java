package babyselfie.mobi.bot;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import babyselfie.mobi.Util.CameraUtil;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;

/**
 * Created by Francisco on 22/10/2016.
 */

public class PicturesRepository {
    private ArrayList<ImageItem> pictures;
    private static PicturesRepository instance;
    private File[] listFile;

    public static PicturesRepository getInstance() {
        if (instance == null)
            instance = new PicturesRepository();
        return instance;
    }

    private PicturesRepository() {
        pictures = new ArrayList<ImageItem>();
    }

    public ImageItem getPicture(int index){
        return this.pictures.get(index);
    }

    public void addPicture(ImageItem imageItem){
        this.pictures.add(imageItem);
    }

    public ArrayList<ImageItem> getList() {
        return this.pictures;
    }

    public ArrayList<ImageItem> getFromSdCard(){
        File diretorioGaleria = CameraUtil.getGalleryDirectory();
        ArrayList<ImageItem> listImagens = new ArrayList<ImageItem>();
        //BitmapFactory.Options options = new BitmapFactory.Options();
        //options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        Bitmap bmp;
        Bitmap resizedBitmap;
        int dstWidth = 960;
        int dstHeight = 960;
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        options.inSampleSize = 2;
        options.inJustDecodeBounds = false;
        options.inTempStorage = new byte[16 * 1024];


        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        if(diretorioGaleria.isDirectory()){
            listFile = diretorioGaleria.listFiles();
        }
        listFile = diretorioGaleria.listFiles();
        for (File f : listFile){
            bmp = BitmapFactory.decodeFile(f.getAbsolutePath(), options);
            resizedBitmap = Bitmap.createScaledBitmap(bmp, dstWidth, dstHeight, false);
            //bmp.compress(Bitmap.CompressFormat.JPEG, 100, bos);
            listImagens.add(new ImageItem(resizedBitmap));
            bmp.recycle();
        }

        /*
        for (int i = 0; i < 1; i++){
            listImagens.add(new ImageItem(BitmapFactory.decodeFile(listFile[i].getAbsolutePath(), options)));
        }
        */
        return listImagens;
    }
}
