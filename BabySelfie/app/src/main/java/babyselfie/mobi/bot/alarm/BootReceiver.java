package babyselfie.mobi.bot.alarm;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import static android.content.Context.MODE_PRIVATE;

/**
 * This BroadcastReceiver automatically (re)starts the alarm when the device is
 * rebooted. This receiver is set to be disabled (android:enabled="false") in the
 * application's manifest file. When the user sets the alarm, the receiver is enabled.
 * When the user cancels the alarm, the receiver is disabled, so that rebooting the
 * device will not trigger this receiver.
 */
// BEGIN_INCLUDE(autostart)
public class BootReceiver extends BroadcastReceiver {
    AlarmReceiver alarm = new AlarmReceiver();
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED"))
        {
            SharedPreferences prefs = context.getSharedPreferences("config", MODE_PRIVATE);
            int frequencia = prefs.getInt("frequencia",1);
            int horas = prefs.getInt("horarioHoras",0);
            int minutos = prefs.getInt("horarioMinutos",0);
            alarm.setAlarm(context,horas,minutos,frequencia);
        }
    }
}
//END_INCLUDE(autostart)
