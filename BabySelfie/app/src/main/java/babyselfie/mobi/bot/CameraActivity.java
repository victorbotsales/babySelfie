/*
 * Copyright 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package babyselfie.mobi.bot;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

import com.R;
import babyselfie.mobi.Util.CameraUtil;

public class CameraActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i("Startando ", "activity camera");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        mediaScanIntent.setData(Uri.fromFile(CameraUtil.getGalleryDirectory()));
        if (null == savedInstanceState) {
            getFragmentManager().beginTransaction()
                    .replace(R.id.container, Camera2BasicFragment.newInstance())
                    .commit();
        }

    }
    /*
    *Alguma coisa do código da câmera buga qd faz isso, entao vou fazer d outra forma
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK){
            switch(requestCode){
                case Constants.REQUEST_CONFIRM_PHOTO:
                    if(data.getStringExtra(Constants.PHOTO_RESULT) == Constants.CODE_ACCEPT_PHOTO){
                        Intent it = new Intent(CameraActivity.this, MenuActivity.class);
                        startActivity(it);
                        finish();
                    }
                    else if(data.getStringExtra(Constants.PHOTO_RESULT) == Constants.CODE_ANOTHER_PHOTO){

                    }
                break;
            }
        }
    }
    */
}
