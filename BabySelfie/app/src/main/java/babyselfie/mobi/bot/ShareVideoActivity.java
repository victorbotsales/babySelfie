package babyselfie.mobi.bot;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.R;
import babyselfie.mobi.Util.CameraUtil;
import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.share.model.ShareVideo;
import com.facebook.share.model.ShareVideoContent;
import com.facebook.share.widget.ShareButton;
import com.facebook.share.widget.ShareDialog;

import java.util.Arrays;

/**
 * Created by Francisco on 09/03/2017.
 */

public class ShareVideoActivity extends AppCompatActivity {
    private CallbackManager callbackManager;
    private ShareDialog shareDialog;
    private ShareButton mShareButton;




    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_reproduzir_video);
        setVideoShare();
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);

    }
    private void setVideoShare() {

        FacebookSdk.sdkInitialize(getApplicationContext());
//        facebook
        callbackManager = CallbackManager.Factory.create();
        shareDialog = new ShareDialog(this);
        LoginManager.getInstance().logInWithPublishPermissions(
                this,
                Arrays.asList("publish_actions"));
//        shareDialog.registerCallback();

        Uri videoUrl = Uri.parse("file://" + CameraUtil.getVideosDirectory().getPath() + "/video.mp4");
        ShareVideo video = new ShareVideo.Builder()
                .setLocalUrl(videoUrl)
                .build();
        final ShareVideoContent content = new ShareVideoContent.Builder()
                .setVideo(video)
                .build();

//        shareDialog.show(content);

        mShareButton = (ShareButton) findViewById(R.id.btFbShare);
        mShareButton.setShareContent(content);
    }


}
