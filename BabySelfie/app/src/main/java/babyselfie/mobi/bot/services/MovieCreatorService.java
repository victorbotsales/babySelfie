package babyselfie.mobi.bot.services;

import android.app.IntentService;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.R;
import babyselfie.mobi.Util.CameraUtil;
import babyselfie.mobi.bot.Tela9VideoActivity;

import org.bytedeco.javacpp.avcodec;
import org.bytedeco.javacpp.opencv_core;
import org.bytedeco.javacv.FFmpegFrameRecorder;
import org.bytedeco.javacv.Frame;
import org.bytedeco.javacv.OpenCVFrameConverter;

import java.io.File;

import static org.bytedeco.javacpp.opencv_imgcodecs.cvLoadImage;

/**
 * Created by Bot on 07/03/2017.
 */

public class MovieCreatorService extends IntentService {

    private int imageHeight;
    private int imageWidth;
    private ProgressDialog progressDialog;

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     */
    public MovieCreatorService() {
        super("MovieCreatorService");
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    protected void onHandleIntent(Intent intent) {
//        onPreExecute();
        createMovie();
        Log.v("Sucesso","video criado com sucesso!");
        onPostExecute();
    }


    protected String createMovie() {

//        File folderAll = Environment
//                .getExternalStorageDirectory();
        File folderBabySelfie = CameraUtil.getGalleryDirectory();

//        File folderMovies = Environment
//                .getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES);

        String path = CameraUtil.getVideosDirectory().getPath();

        File[] listOfFiles = folderBabySelfie.listFiles();
        int imgCounter = listOfFiles.length;
//        File[] listOfFiles2 = folderMovies.listFiles();
//        int imgCounter = 0;
//        System.out.println("listOfFiles size::" + listOfFiles.length);
//        if (listOfFiles.length > 0) {
//
//
//            for (int j = 0; j < listOfFiles.length; j++) {
//
//                if (listOfFiles[j].getAbsolutePath().endsWith(".JPEG")
//                        || listOfFiles[j].getAbsolutePath().endsWith(
//                        ".jpeg")
//                        || listOfFiles[j].getAbsolutePath()
//                        .endsWith(".JPG")
//                        || listOfFiles[j].getAbsolutePath()
//                        .endsWith(".jpg")
//                        || listOfFiles[j].getAbsolutePath()
//                        .endsWith(".png")) {
//                    System.out.println(j + " position listOfFiles::"
//                            + listOfFiles[j].getAbsolutePath());
//                    Bitmap bmp = BitmapFactory.decodeFile(listOfFiles[j]
//                            .getAbsolutePath());
//
//                    if (bmp != null) {
//                        imageHeight = bmp.getHeight();
//                        imageWidth = bmp.getWidth();
//                        System.out.println("bmp is not null");
//                        ++imgCounter;
//                    }
//
//                }
//
//            }
//
//        }

        Log.v("pathReal", path);
        FFmpegFrameRecorder recorder = new FFmpegFrameRecorder(path + "/video.mp4", 768, 1024);
        OpenCVFrameConverter.ToIplImage converter = new OpenCVFrameConverter.ToIplImage();


        try {
            recorder.setVideoCodec(avcodec.AV_CODEC_ID_H264);
            recorder.setVideoOption("preset", "ultrafast");
            recorder.setVideoQuality(10);
            recorder.setFormat("mp4");
            recorder.setFrameRate(1);
            recorder.setPixelFormat(org.bytedeco.javacpp.avutil.AV_PIX_FMT_YUV420P);
            recorder.start();

//            long imageTime = 50000000;
            for (int i = 0; i < imgCounter; i++) {
                opencv_core.IplImage img = cvLoadImage(listOfFiles[i].getAbsolutePath());
                  /*
                    Fazendo resize da imagem para a resolução do vídeo de 1024 x 768 criado! ~~ não
                    é necessário, mas deve facilitar o processamento pois a imagem original tem um
                    tamanho muito grande!
                 */
//                IplImage dest = cvCreateImage(cvSize(768, 1024), img.depth(), img.nChannels());
//                cvResize(img, dest, CV_INTER_NN);

                Log.v("recordtimeStamp", recorder.getTimestamp() + "");
                /*
                  Frame da imagem resized para 1024x768
                 */
//                Frame frame = converter.convert(dest);
                /*
                  Frame da imagem orignal
                 */
                Frame frame = converter.convert(img);

                recorder.record(frame);

            }
            recorder.stop();
//            recorder.release();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // TODO Fazer isso performatico, essa ñ é a forma recomendada
            //sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.parse("file://" + Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES))));
        }
        return "video.mp4";
    }

    protected void onPreExecute() {
        progressDialog = new ProgressDialog(this, R.style.AppTheme_Dark_Dialog);
        progressDialog.setTitle(this.getString(R.string.carregando_video));
        progressDialog.setMessage(this.getString(R.string.espere_por_favor));
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        progressDialog.show();
    }

    protected void onPostExecute() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
//        Intent it = new Intent(this,Tela9VideoActivity.class);
//        this.startActivity(it);

        Intent dialogIntent = new Intent(this, Tela9VideoActivity.class);
        dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(dialogIntent);
    }
}
