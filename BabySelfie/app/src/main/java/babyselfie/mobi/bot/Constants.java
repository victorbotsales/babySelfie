package babyselfie.mobi.bot;

/**
 * Created by Francisco on 22/10/2016.
 */

public class Constants {
    public static final int REQUEST_IMAGE_CAPTURE = 0001;
    public static final int  REQUEST_TAKE_PHOTO = 0002;
    public static final String IMAGES_DIR = "";
    public static final String URI_STRING_CODE = "PHOTO_URI";
    public static final String PHOTO_RESULT = "PHOTO_RESULT";
    public static final int REQUEST_CONFIRM_PHOTO = 0003;
    public static final String CODE_ANOTHER_PHOTO = "another";
    public static final String CODE_ACCEPT_PHOTO = "ok";
    public static final String ALTERAR_FREQUENCIA = "alterarFrequencia";
    public static final String IMG_POSITION = "imagemClicada";

    public static class Extra {
        public static final String FRAGMENT_INDEX = "com.nostra13.example.universalimageloader.FRAGMENT_INDEX";
        public static final String IMAGE_POSITION = "com.nostra13.example.universalimageloader.IMAGE_POSITION";
    }
}
