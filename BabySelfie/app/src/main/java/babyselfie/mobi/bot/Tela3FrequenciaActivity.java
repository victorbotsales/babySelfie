package babyselfie.mobi.bot;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.R;
import babyselfie.mobi.circularseekbar.CircularSeekBar;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class Tela3FrequenciaActivity extends AppCompatActivity {

    private Button btProximo;
    private TextView tvProgressoCircular;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Hanken-Light.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
        setContentView(R.layout.activity_tela3frequencia);
        final CircularSeekBar seekbar = (CircularSeekBar) findViewById(R.id.circularSeekBar1);
        seekbar.setMax(99);
        // Inicializando o circularSeekBar
        Typeface custom_font = Typeface.createFromAsset(getAssets(), "fonts/AllerDisplay.ttf");
        tvProgressoCircular = (TextView) findViewById(R.id.tvProgressoCircular);
        tvProgressoCircular.setText(Integer.toString(seekbar.getProgress() + 1) + " " + getResources().getString(R.string.dia));
        tvProgressoCircular.setTypeface(custom_font);
        seekbar.setOnSeekBarChangeListener(new Tela3FrequenciaActivity.CircleSeekBarListener());

        btProximo = (Button) findViewById(R.id.btProximo);
        btProximo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences prefs = getSharedPreferences("config", MODE_PRIVATE);
                SharedPreferences.Editor editor = prefs.edit();
                editor.putInt("frequencia",seekbar.getProgress() + 1);
                Log.v("frequencia", Integer.toString(seekbar.getProgress() + 1));
                editor.commit();

                Intent it = new Intent(Tela3FrequenciaActivity.this, Tela4HorarioActivity.class);

                if (getIntent().hasExtra(Constants.ALTERAR_FREQUENCIA)){
                    it.putExtra(Constants.ALTERAR_FREQUENCIA,getIntent().getBooleanExtra(Constants.ALTERAR_FREQUENCIA, false));
                }


                startActivity(it);
            }
        });

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    public class CircleSeekBarListener implements CircularSeekBar.OnCircularSeekBarChangeListener {
        @Override
        public void onProgressChanged(CircularSeekBar circularSeekBar, int progress, boolean fromUser) {
            if(progress==0){
                tvProgressoCircular.setText(Integer.toString(progress + 1) + " " + getResources().getString(R.string.dia));
            }
            else{
                tvProgressoCircular.setText(Integer.toString(progress + 1) + " " + getResources().getString(R.string.dias));
            }


        }

        @Override
        public void onStopTrackingTouch(CircularSeekBar seekBar) {

        }

        @Override
        public void onStartTrackingTouch(CircularSeekBar seekBar) {

        }
    }

}
