package babyselfie.mobi.bot;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.R;

import java.util.ArrayList;

/**
 * Created by Francisco on 22/10/2016.
 */

public class GaleriaAdapter extends ArrayAdapter {
    private Context context;
    private int layoutResID;
    private ArrayList<ImageItem> data = new ArrayList<ImageItem>();
    private int imageWidthColumn = 0;

    public GaleriaAdapter(Context context, int layoutResID, ArrayList data){
        super(context, layoutResID, data);
        this.context = context;
        this.layoutResID = layoutResID;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        View row = convertView;
        ViewHolder holder = null;


        if(row == null){
            if(imageWidthColumn==0){

                /*
                    Não funcionou da forma desejada, não sei por que motivo. Se ocorrer
                    outro "bug", tentar utilizar esse método descrito aqui:
                    http://stackoverflow.com/questions/15261088/gridview-with-two-columns-and-auto-resized-images
                 */
//                DisplayMetrics displayMetrics = Resources.getSystem().getDisplayMetrics();
//                int dpWidth = (int)(displayMetrics.widthPixels / displayMetrics.density + 0.5);
                int displayMetrics = Resources.getSystem().getDisplayMetrics().widthPixels;
                imageWidthColumn = displayMetrics / 3;
            }
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(layoutResID, parent, false);
            holder = new ViewHolder();
            holder.image = (ImageView) row.findViewById(R.id.ivBebeSelfie);
            holder.image.setLayoutParams(new GridView.LayoutParams(imageWidthColumn, imageWidthColumn));
            holder.image.setScaleType(ImageView.ScaleType.CENTER_CROP);
            row.setTag(holder);
        }
        else{
            holder = (ViewHolder) row.getTag();
        }

        ImageItem item = data.get(position);
        holder.image.setImageBitmap(item.getImage());
        return row;
    }

    static class ViewHolder {
        ImageView image;
    }
}
