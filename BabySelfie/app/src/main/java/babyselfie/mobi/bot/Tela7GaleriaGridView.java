package babyselfie.mobi.bot;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;

import com.R;

import java.util.ArrayList;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class Tela7GaleriaGridView extends AppCompatActivity {

    private GridView mGridview;
    private GaleriaAdapter mGaleriaAdapter;
    private Button mBtnGerarFilme;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Hanken-Light.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
        setContentView(R.layout.activity_tela7galeria);

        //getData();
        mGridview = (GridView) findViewById(R.id.gvGaleria);

        // Com holder
        //mGaleriaAdapter = new GaleriaAdapter(this, R.layout.item_galeria, PicturesRepository.getInstance().getList());
//        mGridview.setAdapter(mGaleriaAdapter);

        mGaleriaAdapter = new GaleriaAdapter(this, R.layout.item_galeria, PicturesRepository.getInstance().getFromSdCard());
        // Sem holder
//         mGridview.setAdapter(new ImageAdapter(this));

        mGridview.setAdapter(mGaleriaAdapter);

        mBtnGerarFilme = (Button) findViewById(R.id.btGerar);
//        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/Hanken-Light.ttf");
//        mBtnGerarFilme.setTypeface(typeface);


        mBtnGerarFilme.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MovieCreator teste = new MovieCreator(Tela7GaleriaGridView.this);
                teste.execute("foda-se");
            }
        });
    }

    private ArrayList<ImageItem> getData() {
        final ArrayList<ImageItem> imageItems = new ArrayList<ImageItem>();
        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.sample_0);
        Bitmap bitmap2 = BitmapFactory.decodeResource(getResources(), R.drawable.sample_1);
        Bitmap bitmap3 = BitmapFactory.decodeResource(getResources(), R.drawable.sample_2);
        for(int i = 0; i <=100000; i++){
            PicturesRepository.getInstance().addPicture(new ImageItem(bitmap));
            PicturesRepository.getInstance().addPicture(new ImageItem(bitmap2));
            PicturesRepository.getInstance().addPicture(new ImageItem(bitmap3));
            //imageItems.add(new ImageItem(bitmap));
            //imageItems.add(new ImageItem(bitmap2));
            //imageItems.add(new ImageItem(bitmap3));
        }
        return imageItems;
    }

    private class ItemClickHandler implements AdapterView.OnItemClickListener{

        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            //Todo
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

}
