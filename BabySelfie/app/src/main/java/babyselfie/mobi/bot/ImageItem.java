package babyselfie.mobi.bot;

import android.graphics.Bitmap;

/**
 * Created by Francisco on 22/10/2016.
 */

public class ImageItem {
    private Bitmap image;

    public ImageItem(Bitmap image) {
        super();
        this.image = image;
    }

    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }

}