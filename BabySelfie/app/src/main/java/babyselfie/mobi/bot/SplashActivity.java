package babyselfie.mobi.bot;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.R;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


// TODO Utilizar assim ou do modo de branded-launch-screen ( http://antonioleiva.com/branded-launch-screen/ )
public class SplashActivity extends AppCompatActivity {

    private static boolean splashLoaded = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Hanken-Light.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );


        if (!splashLoaded) {
            setContentView(R.layout.activity_splash);
            int secondsDelayed = 3;
            new Handler().postDelayed(new Runnable() {
                public void run() {
                    startActivity(new Intent(SplashActivity.this, Tela2LoginActivity.class));
                    //startActivity(new Intent(SplashActivity.this, MenuActivity.class));
                    finish();
                }
            }, secondsDelayed * 500);

            // Testar a splash screen...
            // splashLoaded = true;
        }
        else {
            Intent goToMainActivity = new Intent(SplashActivity.this, Tela2LoginActivity.class);
            //Intent goToMainActivity = new Intent(SplashActivity.this, MenuActivity.class);
            goToMainActivity.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            startActivity(goToMainActivity);
            finish();
        }

        /*
            //Modo branded-launch-sreen
            Intent goToMainActivity = new Intent(SplashActivity.this, Tela3FrequenciaActivity.class);
            goToMainActivity.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            startActivity(goToMainActivity);
            finish();
         */
    }

    protected void attachBaseContext(Context context) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(context));
    }


}
