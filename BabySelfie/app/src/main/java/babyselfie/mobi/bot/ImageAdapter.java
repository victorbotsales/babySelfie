package babyselfie.mobi.bot;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.R;

/**
 * Created by Bot on 20/10/2016.
 */

/*
    TODO Utilizar holder? Desempenho
 */
public class ImageAdapter extends BaseAdapter {
    private Context mContext;

    public ImageAdapter(Context c) {
        mContext = c;
    }

    public int getCount() {
        return mThumbIds.length;
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    // create a new ImageView for each item referenced by the Adapter
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView;

        if (convertView == null) {
            // if it's not recycled, initialize some attributes
            imageView = new ImageView(mContext);
            int iDisplayWidth = imageView.getResources().getDisplayMetrics().widthPixels;
            int iImageWidth = iDisplayWidth / 3;
            imageView.setLayoutParams(new GridView.LayoutParams(iImageWidth, iImageWidth));
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        } else {
            imageView = (ImageView) convertView;
        }

        imageView.setImageResource(mThumbIds[position]);

        return imageView;
    }

    // references to our images
    private Integer[] mThumbIds = {
            R.drawable.sample_0, R.drawable.sample_1,
            R.drawable.sample_2, R.drawable.sample_0,
            R.drawable.sample_1, R.drawable.sample_2,
            R.drawable.sample_0, R.drawable.sample_1,
            R.drawable.sample_2,
            R.drawable.sample_0, R.drawable.sample_1,
            R.drawable.sample_2, R.drawable.sample_0,
            R.drawable.sample_1, R.drawable.sample_2,
            R.drawable.sample_0, R.drawable.sample_1,
            R.drawable.sample_2
    };
}