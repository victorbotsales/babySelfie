package babyselfie.mobi.bot;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import com.R;
import babyselfie.mobi.Util.CameraUtil;

import org.bytedeco.javacpp.avcodec;
import org.bytedeco.javacpp.opencv_core;
import org.bytedeco.javacv.FFmpegFrameRecorder;
import org.bytedeco.javacv.Frame;
import org.bytedeco.javacv.OpenCVFrameConverter;

import java.io.File;

import static org.bytedeco.javacpp.opencv_imgcodecs.cvLoadImage;

/**
 * Created by Bot on 09/12/2016.
 */

public class MovieCreator extends AsyncTask<String, Void, Boolean> {
    public MovieCreator(Context context) {
        this.context = context;
        this.contextApp = context.getApplicationContext();
    }

    private final Context context;
    private final Context contextApp;
    private ProgressDialog progressDialog;
    private int imageHeight;
    private int imageWidth;

    protected String createMovie() {

//        File folderAll = Environment
//                .getExternalStorageDirectory();
        File folderBabySelfie = CameraUtil.getGalleryDirectory();

//        File folderMovies = Environment
//                .getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES);

        String path = CameraUtil.getVideosDirectory().getPath();

        File[] listOfFiles = folderBabySelfie.listFiles();
        int imgCounter = listOfFiles.length;

        Log.v("pathReal", path);
        FFmpegFrameRecorder recorder = new FFmpegFrameRecorder("file://" + path + "/video.mp4", 768, 1024);
        OpenCVFrameConverter.ToIplImage converter = new OpenCVFrameConverter.ToIplImage();


        try {
            recorder.setVideoCodec(avcodec.AV_CODEC_ID_H264);
            recorder.setVideoOption("preset", "ultrafast");
            recorder.setVideoQuality(10);
            recorder.setFormat("mp4");
            recorder.setFrameRate(1);
            recorder.setPixelFormat(org.bytedeco.javacpp.avutil.AV_PIX_FMT_YUV420P);
            recorder.start();

//            long imageTime = 50000000;
            for (int i = 0; i < imgCounter; i++) {
                opencv_core.IplImage img = cvLoadImage(listOfFiles[i].getAbsolutePath());
//                long time = (i+1) * imageTime;
//                if (time > recorder.getTimestamp()) {
//                    recorder.setTimestamp(time);
//                }
//                recorder.setTimestamp(recorder.getTimestamp()+3000);
                Log.v("recordtimeStamp", recorder.getTimestamp() + "");
                Frame frame = converter.convert(img);
                recorder.record(frame);

            }
            recorder.stop();
            recorder.release();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // TODO Fazer isso performatico, essa ñ é a forma recomendada
            //sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.parse("file://" + Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES))));
        }
        return "video.mp4";
    }

    @Override
    protected Boolean doInBackground(String... params) {
        String fileName = createMovie();
        boolean result = fileName.isEmpty();
        return true;
    }

    @Override
    protected void onPreExecute() {
        progressDialog = new ProgressDialog(context, R.style.AppTheme_Dark_Dialog);
        progressDialog.setTitle(contextApp.getString(R.string.carregando_video));
        progressDialog.setMessage(contextApp.getString(R.string.espere_por_favor));
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        progressDialog.show();
    }

    @Override
    protected void onPostExecute(Boolean result) {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
        Intent it = new Intent(context,Tela9VideoActivity.class);
        Intent mediaStoreUpdateIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        mediaStoreUpdateIntent.setData(Uri.parse("file://" + CameraUtil.getVideosDirectory().getPath() + "video.mp4"));
        context.sendBroadcast(mediaStoreUpdateIntent);
        context.startActivity(it);
    }
}