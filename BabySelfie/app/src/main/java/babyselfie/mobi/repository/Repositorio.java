package babyselfie.mobi.repository;

/**
 * Created by Francisco on 25/11/2016.
 */

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import babyselfie.mobi.Util.CameraUtil;
import babyselfie.mobi.Util.DirectoryNotFoundException;
import babyselfie.mobi.bot.ImageItem;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class Repositorio implements IRepositorio {
    private ArrayList<ImageItem> pictures;
    private static Repositorio instance;
    private File[] fileList;

    public static IRepositorio getInstance() {
        if (instance == null)
            instance = new Repositorio();
        return instance;
    }

    private Repositorio() {
        pictures = new ArrayList<ImageItem>();
    }

    @Override
    public ImageItem getPicture(int index) {
        return this.pictures.get(index);
    }

    @Override
    public void addPicture(Bitmap bmp) {
        this.pictures.add(new ImageItem(bmp));
    }

    @Override
    public ArrayList<ImageItem> getList() {
        return this.pictures;
    }

//    @Override
//    public ArrayList<ImageItem> getFromSdCard(){
//        File diretorioGaleria = CameraUtil.getGalleryDirectory();
//        ArrayList<ImageItem> listImagens = new ArrayList<ImageItem>();
//        //BitmapFactory.Options options = new BitmapFactory.Options();
//        //options.inPreferredConfig = Bitmap.Config.ARGB_8888;
//        Bitmap bmp;
//        Bitmap resizedBitmap;
//        int dstWidth = 960;
//        int dstHeight = 960;
//        final BitmapFactory.Options options = new BitmapFactory.Options();
//        options.inJustDecodeBounds = true;
//        options.inSampleSize = 2;
//        options.inJustDecodeBounds = false;
//        options.inTempStorage = new byte[16 * 1024];
//
//
//        ByteArrayOutputStream bos = new ByteArrayOutputStream();
//
//        if(diretorioGaleria.isDirectory()){
//            fileList = diretorioGaleria.listFiles();
//        }
//        fileList = diretorioGaleria.listFiles();
//        for (File f : fileList){
//            bmp = BitmapFactory.decodeFile(f.getAbsolutePath(), options);
//            resizedBitmap = Bitmap.createScaledBitmap(bmp, dstWidth, dstHeight, false);
//            //bmp.compress(Bitmap.CompressFormat.JPEG, 100, bos);
//            listImagens.add(new ImageItem(resizedBitmap));
//            bmp.recycle();
//        }
//
//        /*
//        for (int i = 0; i < 1; i++){
//            listImagens.add(new ImageItem(BitmapFactory.decodeFile(fileList[i].getAbsolutePath(), options)));
//        }
//        */
//        return listImagens;
//    }

    @Override
    public ArrayList<ImageItem> getFromSdCard() {
        File diretorioGaleria = CameraUtil.getGalleryDirectory();
        ArrayList<ImageItem> listImagens = new ArrayList<ImageItem>();
        if(diretorioGaleria.isDirectory()){
            fileList = diretorioGaleria.listFiles();
        }
        for(File f: fileList){
            listImagens.add(new ImageItem(CameraUtil.decodeSampledBitmapFromUri(f.getPath(), 400, 400)));
//            decodeSampledBitmapFromUri
        }

        return listImagens;
    }

    @Override
    public Bitmap getImageFromSd(String imageName) {
        Bitmap bmpImage = null;
        File diretorioGaleria = CameraUtil.getGalleryDirectory();
        //String fileName = diretorioGaleria + "/" + imageName;
        //File image = new File(fileName);


        bmpImage = BitmapFactory.decodeFile(imageName);


        return bmpImage;
    }

    @Override
    public boolean deleteImage(String fileName) {
        File image = new File(fileName);
        return image.delete();
    }

    @Override
    public String[] getAllImgUri(){
        File diretorioGaleria = CameraUtil.getGalleryDirectory();
        String[] paths;
        if(diretorioGaleria.isDirectory()){
            fileList = diretorioGaleria.listFiles();
        }
        paths = new String[fileList.length];
        for(int i=0; i<fileList.length; i++){
            paths[i] = "file://" + fileList[i].getPath();
        }


        return paths;
    }

    @Override
    public List<String> deleteAllPictures() throws DirectoryNotFoundException{
        List<String> paths = new ArrayList<>();
        File diretorioGaleria = CameraUtil.getGalleryDirectory();
        if (diretorioGaleria.isDirectory()){
            for (File f : diretorioGaleria.listFiles()){
                if (f.exists()){
                    f.delete();
                    paths.add("file://" + f.getPath());
                }
            }
        }
        else {
            throw new DirectoryNotFoundException("Directory does not exist or cannot be oppen");
        }

        return paths;
    }


    private static void notifySystem(Intent intent, String fileName){

    }

    @Override
    public String getUrl(int position) {
        File diretorioGaleria = CameraUtil.getGalleryDirectory();
        if(diretorioGaleria.isDirectory()){
            fileList = diretorioGaleria.listFiles();
        }
        return fileList[position].toString();
    }
}