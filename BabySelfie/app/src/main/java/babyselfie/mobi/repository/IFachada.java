package babyselfie.mobi.repository;

import android.graphics.Bitmap;

import babyselfie.mobi.Util.DirectoryNotFoundException;
import babyselfie.mobi.bot.ImageItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Francisco on 25/11/2016.
 */

public interface IFachada {
    public ArrayList<ImageItem> getImageListFromSd();
    public Bitmap getImageFromSd(String fileName);
    public boolean deleteImage(String fileName);
    public List<String> deleteAllPictures() throws DirectoryNotFoundException;
}
