package babyselfie.mobi.repository;

import android.graphics.Bitmap;

import babyselfie.mobi.Util.DirectoryNotFoundException;
import babyselfie.mobi.bot.ImageItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Francisco on 25/11/2016.
 */

public interface IRepositorio {
    ImageItem getPicture(int index);
    void addPicture(Bitmap bmp);
    ArrayList<ImageItem> getList();
    ArrayList<ImageItem> getFromSdCard();
    Bitmap getImageFromSd(String imageName);
    boolean deleteImage(String fileName);
    String[] getAllImgUri();
    List<String> deleteAllPictures() throws DirectoryNotFoundException;
    String getUrl(int position);
}
