package babyselfie.mobi.repository;

import android.graphics.Bitmap;

import babyselfie.mobi.Util.DirectoryNotFoundException;
import babyselfie.mobi.bot.ImageItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Francisco on 25/11/2016.
 */

public class Fachada implements IFachada{

    public Fachada(){
    }

    @Override
    public ArrayList<ImageItem> getImageListFromSd() {
        return Repositorio.getInstance().getFromSdCard();
    }

    @Override
    public Bitmap getImageFromSd(String fileName) {
        return Repositorio.getInstance().getImageFromSd(fileName);
    }

    @Override
    public boolean deleteImage(String fileName){
        return Repositorio.getInstance().deleteImage(fileName);
    }

    public List<String> deleteAllPictures() throws DirectoryNotFoundException{
        return Repositorio.getInstance().deleteAllPictures();
    }
}
