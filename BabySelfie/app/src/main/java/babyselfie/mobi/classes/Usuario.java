package babyselfie.mobi.classes;

import android.provider.ContactsContract;

/**
 * Created by Francisco on 07/11/2016.
 */

public class Usuario {
    private String pNome;
    private String uNome;
    private ContactsContract.CommonDataKinds.Email email;

    public Usuario(String pNome, String uNome, ContactsContract.CommonDataKinds.Email email) {
        this.pNome = pNome;
        this.uNome = uNome;
        this.email = email;
    }
}
