package com.example.bot.babyselfie;

import android.app.Dialog;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.R;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import babyselfie.mobi.bot.Constants;
import babyselfie.mobi.repository.Repositorio;
//import com.repository.Repositorio;

/**
 * Created by Francisco on 24/04/2017.
 */

public class PhotoDetailActivity extends AppCompatActivity {
    private ImageView mIvFotoClicada;
    private FloatingActionButton mFabDetalhe;
    private Dialog detailDialog;
    private TextView mTvDataCriacao;
    private TextView mTvTamanho;
    private TextView mTvCaminho;
    private String photoUrl;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_detail);
        int position = getIntent().getIntExtra(Constants.IMG_POSITION, 0);
        photoUrl = Repositorio.getInstance().getUrl(position);
        mIvFotoClicada = (ImageView) findViewById(R.id.ivFotoClicada);
        mIvFotoClicada.setImageBitmap(BitmapFactory.decodeFile(photoUrl));
        mFabDetalhe = (FloatingActionButton) findViewById(R.id.fabDetalhe);

        //Ocultar a Status bar
        final View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);

        mFabDetalhe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                detailDialog.show();
            }
        });

        setUpDialog();
    }

    private void setUpDialog() {
        File f = new File(photoUrl);
        detailDialog = new Dialog(this);
        detailDialog.setContentView(R.layout.dialog_photo_detail);
        mTvCaminho = (TextView) detailDialog.findViewById(R.id.tvCaminho);
        mTvDataCriacao = (TextView) detailDialog.findViewById(R.id.tvDataCriacao);
        mTvTamanho = (TextView) detailDialog.findViewById(R.id.tvTamanho);

        mTvCaminho.setText(mTvCaminho.getText() + ": " + photoUrl);

        Date d = new Date();
        d.setTime(f.lastModified());


        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy hh:mm");
        mTvDataCriacao.setText(mTvDataCriacao.getText() + ": " + sdf.format(d));

        //mTvTitulo.setText();

        mTvTamanho.setText(mTvTamanho.getText() + ": " + f.length()/1000 + " KB");

    }


}
